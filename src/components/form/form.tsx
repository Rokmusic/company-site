import React, { FC, useState } from 'react';

import { setIsOpen } from '@/store/slices/modalSlice';
import { useAppDispatch } from '@/hooks/useAppDispatch';
import FormBlock from '@/components/blocks/formBlock/formBlock';
import FormSuccessBlock from '@/components/blocks/formSuccessBlock/formSuccessBlock';

interface IFormProps {
  headline?: string;
}

const Form: FC<IFormProps> = ({ headline }) => {
  const [success, setSuccess] = useState<boolean>(false);
  const withHeadline = headline ? headline?.length > 0 : false;

  const dispatch = useAppDispatch();
  function onClickHandlerCross() {
    dispatch(setIsOpen(false));
  }
  return (
    <>
      {success ? (
        <FormSuccessBlock withHeadline={withHeadline} onClickHandlerCross={onClickHandlerCross} />
      ) : (
        <FormBlock
          headline={headline}
          setSuccess={setSuccess}
          onClickHandlerCross={onClickHandlerCross}
        />
      )}
    </>
  );
};

export default Form;

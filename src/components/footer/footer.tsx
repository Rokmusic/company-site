import React, { FC } from 'react';

import Link from 'next/link';

import styles from './footer.module.css';

import Logo from '@/components/icons/logo';
import FooterMenu from '@/components/footerMenu/footerMenu';
import { getTelHref } from '@/utils/getTelHref';
import { contacts } from '@/constans/contacts';

const Footer: FC = () => {
  return (
    <footer className={styles.footer}>
      <div className={styles.containerFooter}>
        <div className={styles.company}>
          <div className={styles.logoContainer}>
            <Link href={'/'}>
              <Logo />
            </Link>
          </div>
          <div className={styles.companyCopyright}>
            <div>
              {`© 2014-${new Date().getFullYear().toString().substring(2, 4)}. ИП Турчанинов В.В.`}
            </div>
            <div>ИНН 87645873645</div>
            <div>Все права защищены</div>
          </div>
        </div>
        <FooterMenu />
        <div className={styles.contacts}>
          <a href={getTelHref(contacts.tel)}>{contacts.tel}</a>
          {contacts.emails.map((email, index) => (
            <a key={index} href={`email:${email}`}>
              {email}
            </a>
          ))}
        </div>
      </div>
      <div className={styles.policy}>
        <Link href={'/policy'}>Политика обработки персональных данных</Link>
      </div>
    </footer>
  );
};

export default Footer;

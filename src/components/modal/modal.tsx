import React, { FC, useEffect, useState } from 'react';

import { useAppSelector } from '@/hooks/useAppSelector';
import { modalStateSelector } from '@/store/selectors/modalSelectors';
import ModalBlock from '@/components/blocks/modalBlock/modalBlock';

const Modal: FC = () => {
  const modalState = useAppSelector(modalStateSelector);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  useEffect(() => {
    setIsOpen(modalState);
  }, [modalState]);

  return <>{isOpen && <ModalBlock />}</>;
};

export default Modal;

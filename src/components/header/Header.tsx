import React, { FC } from 'react';
import Link from 'next/link';

import styles from './header.module.css';

import Menu from '@/components/menu/menu';
import Logo from '@/components/icons/logo';

import { contacts } from '@/constans/contacts';
import { getTelHref } from '@/utils/getTelHref';

const Header: FC = () => {
  return (
    <header>
      <div className={styles.headerContent}>
        <div className={styles.logoContainer}>
          <Link href={'/'} className={styles.logoLink}>
            <Logo />
          </Link>
        </div>
        <Menu />
        <div className={styles.contacts}>
          <a href={getTelHref(contacts.tel)}>{contacts.tel}</a>
        </div>
      </div>
    </header>
  );
};

export default Header;

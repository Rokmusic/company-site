import React, { FC } from 'react';

import styles from '@/components/pages/casesPage/casesPage.module.css';
import ButtonTab from '@/components/buttons/buttonTab/buttonTab';
import CasesMasonryBlock from '@/components/blocks/casesMasonryBlock/casesMasonryBlock';
import ContainerFull from '@/components/containerFull/containerFull';
import { TCase } from '@/types/cases.type';

const rootUrl = 'cases/';

interface ICasesPageProps {
  cards: Array<TCase>;
}

const CasesPage: FC<ICasesPageProps> = ({ cards }) => {
  return (
    <ContainerFull bgColor={'#111929'}>
      <section className={styles.section}>
        <h2 className={styles.headline}>кейсотека</h2>
        <div className={styles.buttons}>
          <ButtonTab name={'Все'} rootUrl={rootUrl} slug={''} isActive={true} />
          <ButtonTab
            name={'Автоматизация'}
            rootUrl={rootUrl}
            slug={'automation-of-business-processes'}
          />
          <ButtonTab
            name={'Цифровые сервисы'}
            rootUrl={rootUrl}
            slug={'development-of-digital-services'}
          />
          <ButtonTab
            name={'IoT'}
            rootUrl={rootUrl}
            slug={'development-of-software-and-hardware-solutions_iot'}
          />
        </div>
        {cards ? (
          <div className={styles.cases}>
            <CasesMasonryBlock cards={cards} />
          </div>
        ) : (
          <>loader</>
        )}
      </section>
    </ContainerFull>
  );
};

export default CasesPage;

import React, { FC, useRef, useState } from 'react';

import styles from './menu.module.css';

import { menuItems } from '@/constans/menuData';
import { contacts } from '@/constans/contacts';
import { getTelHref } from '@/utils/getTelHref';
import MenuItem from '@/components/menu/menuItem';
import Button from '@/components/buttons/button/button';
import Phone from '@/components/icons/phone';
import Email from '@/components/icons/email';
import Location from '@/components/icons/location';
import Burger from '@/components/icons/burger';
import { useClickOutside } from '@/hooks/useClickOutside';

const Menu: FC = () => {
  const menuRef = useRef(null);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const clickOutsideHandler = () => {
    setIsOpen(false);
  };
  useClickOutside(menuRef, clickOutsideHandler);
  return (
    <div ref={menuRef} className={styles.navContainer}>
      <Burger isOpenMenu={isOpen} setIsOpenMenu={setIsOpen} classname={styles.burger} />
      <nav className={styles.nav}>
        <ul className={styles.menuList}>
          {menuItems.map((menuItem, idx) => (
            <MenuItem key={idx} menuItem={menuItem} setIsOpenMenu={setIsOpen} />
          ))}
        </ul>
        <div className={styles.menuBottom}>
          <Button text={'ОБСУДИТЬ ПРОЕКТ'} width={'full'} />
          <div className={styles.contacts}>
            <a href={getTelHref(contacts.tel)}>
              <Phone />
              {contacts.tel}
            </a>
            {contacts.emails.map((email, index) => (
              <a key={index} href={`email:${email}`}>
                <Email />
                {email}
              </a>
            ))}
            <a href={'#'}>
              <Location />
              {contacts.address}
            </a>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Menu;

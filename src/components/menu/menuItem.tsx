import React, { FC, Dispatch, SetStateAction, useState } from 'react';
import Link from 'next/link';

import styles from './menuItem.module.css';

import { TMenuItem } from '@/types/menu.type';
import Chevron from '@/components/icons/chevron';

interface IMenuItem {
  menuItem: TMenuItem;
  setIsOpenMenu: Dispatch<SetStateAction<boolean>>;
}

const MenuItem: FC<IMenuItem> = ({ menuItem, setIsOpenMenu }) => {
  const [needShow, setNeedShow] = useState<boolean>(false);
  const onClickMenuItemHandler = () => {
    setIsOpenMenu(false);
  };
  const onMouseEnterHandler = () => {
    setNeedShow(true);
  };
  const onMouseLeaveHandler = () => {
    setNeedShow(false);
  };
  if (menuItem.items?.length) {
    return (
      <li
        key={menuItem.id}
        className={`${needShow ? styles.needShowSubMenu : ''} ${styles.menuWithSubItems}`}
        onMouseLeave={onMouseLeaveHandler}
        onMouseEnter={onMouseEnterHandler}
      >
        {menuItem.name}
        <Chevron />
        <div className={`${styles.containerListWithSubItems}`}>
          <ul className={styles.menuListWithSubItems}>
            {menuItem.items.map((subItem) => (
              <li key={subItem.id} className={styles.subItem}>
                <div className={styles.subItemContainer}>
                  {subItem.icon}
                  <br />
                  <Link
                    href={`/${subItem.url}`}
                    className={styles.subItemLink}
                    onClick={onClickMenuItemHandler}
                  />
                  {subItem.name}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </li>
    );
  }
  return (
    <li key={menuItem.id} className={styles.menuItem}>
      <Link
        className={styles.itemLink}
        href={menuItem.url.includes('#') ? menuItem.url : `/${menuItem.url}`}
        onClick={onClickMenuItemHandler}
      >
        {menuItem.name}
      </Link>
    </li>
  );
};

export default MenuItem;

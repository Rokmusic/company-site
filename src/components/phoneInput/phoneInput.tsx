import React, { FC } from 'react';
import InputMask from 'react-input-mask';

interface IPhoneInputProps {
  text: string;
  setTextHandler: (event: string) => void;
  className?: string;
}

const PhoneInput: FC<IPhoneInputProps> = ({ text, setTextHandler, className }) => {
  return (
    <InputMask
      required
      className={className}
      type={'tel'}
      name={'tel'}
      value={text}
      onChange={(e) => setTextHandler(e.currentTarget.value)}
      mask="+9 (999) 999 99 99"
      placeholder={'+_ (___) ___ __ __'}
      // alwaysShowMask
    />
  );
};

export default PhoneInput;

import React, { FC } from 'react';

import Link from 'next/link';

import styles from './footerMenuItem.module.css';

import { TMenuItem } from '@/types/menu.type';

interface IFooterMenuItemProps {
  menuItem: TMenuItem;
}

const FooterMenuItem: FC<IFooterMenuItemProps> = ({ menuItem }) => {
  if (menuItem.items?.length) {
    return (
      <li className={styles.menuItemWithSubItems}>
        {menuItem.name}
        <ul
          className={styles.listWithSubItems}
          style={{ maxHeight: `${10 * menuItem.items?.length + 10}px` }}
        >
          {menuItem.items.map((subItem) => (
            <li key={subItem.id} className={styles.subItem}>
              <Link href={`/${subItem.url}`} className={styles.subItemLink}>
                {subItem.name}
              </Link>
            </li>
          ))}
        </ul>
      </li>
    );
  }
  return (
    <li key={menuItem.id} className={styles.menuItem}>
      <Link href={menuItem.url.includes('#') ? menuItem.url : `/${menuItem.url}`}>
        {menuItem.name}
      </Link>
    </li>
  );
};

export default FooterMenuItem;

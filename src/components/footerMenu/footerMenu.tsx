import React, { FC } from 'react';

import styles from './footerMenu.module.css';

import { menuItems } from '@/constans/menuData';
import FooterMenuItem from '@/components/footerMenu/footerMenuItem';

const FooterMenu: FC = () => {
  return (
    <nav className={styles.footerNav}>
      <ul className={styles.footerNavList}>
        {menuItems.map((menuItem, idx) => (
          <FooterMenuItem key={idx} menuItem={menuItem} />
        ))}
      </ul>
    </nav>
  );
};

export default FooterMenu;

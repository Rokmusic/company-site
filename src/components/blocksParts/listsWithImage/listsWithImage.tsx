import Link from 'next/link';
import React, { FC } from 'react';

import styles from './listsWithImage.module.css';

import { IListWithImage } from '@/types/lists.types';
import List from '@/components/list/list';

const ListsWithImage: FC<IListWithImage> = ({ data }) => {
  return (
    <>
      {data.map((item, idx) => (
        <section key={idx} className={styles.section}>
          <span
            className={styles.imageDecor}
            style={{ backgroundImage: `url(${item.imageSrc})` }}
          />
          <h3 className={styles.headline}>{item.headline}</h3>
          <div>
            <List listItems={item.listItems} />
            {item.link && (
              <Link href={item.link} className={styles.link}>
                ПОДРОБНЕЕ
              </Link>
            )}
          </div>
        </section>
      ))}
    </>
  );
};

export default ListsWithImage;

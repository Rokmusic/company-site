import React, { FC } from 'react';
import { useRouter } from 'next/router';

import styles from './button.module.css';

import { useAppDispatch } from '@/hooks/useAppDispatch';
import { setIsOpen } from '@/store/slices/modalSlice';

interface IButtonProps {
  text: string;
  width: 'full' | number;
  type?: 'submit';
  href?: string;
}

const Button: FC<IButtonProps> = ({ text, width, type, href = null }) => {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const onClickHandler = (
    event:
      | React.MouseEvent<HTMLAnchorElement, MouseEvent>
      | React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    if (type === 'submit') return;
    if (href) {
      event.preventDefault();
      return router.replace(href);
    }
    dispatch(setIsOpen(true));
  };
  const As = href ? 'a' : 'button';
  return (
    <As
      type={type && !href ? type || 'button' : undefined}
      style={{ width: width === 'full' ? '100%' : `${width}px` }}
      className={styles.button}
      onClick={(e) => onClickHandler(e)}
      href={href ? href : undefined}
    >
      {text}
    </As>
  );
};

export default Button;

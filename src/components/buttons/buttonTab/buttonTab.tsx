import React, { FC } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import styles from './buttonTab.module.css';

import { TTabs } from '@/types/menu.type';

interface IButtonTabProps {
  name: string;
  rootUrl: string;
  slug: TTabs | '';
  isActive?: boolean;
}

const ButtonTab: FC<IButtonTabProps> = ({ name, rootUrl, slug, isActive }) => {
  const router = useRouter();
  const splitPath = router.asPath.split('/');
  const activeTab = splitPath[splitPath.length - 1];
  const activeButton = activeTab === slug || (activeTab === 'cases' && isActive);
  const As = activeButton ? 'h1' : 'span';
  return (
    <As className={`${styles.button} ${activeButton ? styles.activeButton : ''}`}>
      <Link className={styles.link} href={`/${rootUrl + slug}`}>
        {name}
      </Link>
    </As>
  );
};

export default ButtonTab;

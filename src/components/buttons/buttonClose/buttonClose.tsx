import React, { FC } from 'react';

import styles from '@/components/blocks/formBlock/formBlock.module.css';
import Cross from '@/components/icons/cross';

interface IButtonCloseProps {
  onClickHandler: () => void;
}

const ButtonClose: FC<IButtonCloseProps> = ({ onClickHandler }) => {
  return (
    <button className={styles.buttonClose} onClick={onClickHandler}>
      <Cross />
    </button>
  );
};

export default ButtonClose;

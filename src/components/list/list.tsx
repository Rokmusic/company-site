import React, { FC } from 'react';

import styles from './list.module.css';

interface IListProps {
  listItems: Array<string>;
  whiteDots?: boolean;
}

const List: FC<IListProps> = ({ listItems, whiteDots }) => {
  return (
    <ul className={styles.list}>
      {listItems.map((listItem, idx) => (
        <li key={idx} className={`${styles.listItem} ${whiteDots ? styles.whiteDots : ''}`}>
          {listItem}
        </li>
      ))}
    </ul>
  );
};

export default List;

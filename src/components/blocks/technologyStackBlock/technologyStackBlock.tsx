import React, { FC } from 'react';

import styles from './technologyStackBlock.module.css';

import FullImageBlock from '@/components/blocks/fullImageBlock/fullImageBlock';
import ContainerFull from '@/components/containerFull/containerFull';
import useMediaQuery from '@/hooks/useMediaQuery';

const TechnologyStackBlock: FC = () => {
  const isMobile = useMediaQuery('(max-width: 576px)');
  const languagesImgSrc = isMobile
    ? '/files/images/programming-languages-mobile.webp'
    : '/files/images/programming-languages.webp';
  const frameworksImgSrc = isMobile
    ? '/files/images/frameworks-mobile.webp'
    : '/files/images/frameworks.webp';
  const inWorksImgSrc = isMobile
    ? '/files/images/in-works-mobile.webp'
    : '/files/images/in-works.webp';
  return (
    <ContainerFull bgColor={'#F5F7F9'}>
      <section className={styles.section}>
        <h2 className={styles.headline}>СТЕК ТЕХНОЛОГИЙ</h2>
        <span className={styles.span}>
          Мы ведем разработку под Linux или используем кроссплатформенные решения
        </span>
        <div className={styles.containerImages}>
          <div className={styles.languages}>
            <FullImageBlock
              headline={'Языки программирования'}
              imageSrc={languagesImgSrc}
              alt={'Языки программирования'}
            />
          </div>
          <div className={styles.frameworks}>
            <FullImageBlock
              headline={'Фреймворки'}
              imageSrc={frameworksImgSrc}
              alt={'Фреймворки'}
            />
          </div>
          <div className={styles.inWorks}>
            <FullImageBlock
              headline={'В работе используем'}
              imageSrc={inWorksImgSrc}
              alt={'В работе используем'}
            />
          </div>
        </div>
      </section>
    </ContainerFull>
  );
};

export default TechnologyStackBlock;

import React, { FC } from 'react';

import styles from './heroBlock.module.css';

import Button from '@/components/buttons/button/button';
import ContainerFull from '@/components/containerFull/containerFull';
import List from '@/components/list/list';
import { TMenuSubItems } from '@/types/menu.type';

type TListTexts = {
  headline: string;
  items: Array<string>;
};

type TTexts = {
  paragraph: Array<string>;
  list: TListTexts;
};

type THeroTextItems = Pick<TMenuSubItems, 'id' | 'name' | 'icon'>;

interface IHeroProps {
  headline: string;
  texts?: TTexts;
  subItems: Array<TMenuSubItems> | Array<THeroTextItems> | { imageSrc: string; alt: string };
  bgImageStyle?: string;
  bgColor?: string;
  withoutPaddingTop?: boolean;
}

const HeroBlock: FC<IHeroProps> = ({
  headline,
  texts,
  subItems,
  withoutPaddingTop,
  bgImageStyle,
  bgColor,
}) => {
  return (
    <ContainerFull bgImageStyle={bgImageStyle} bgColor={bgColor}>
      <section className={styles.sectionHero} style={withoutPaddingTop ? { paddingTop: 0 } : {}}>
        <div className={styles.firstContainer}>
          <h1 className={styles.headline_1}>{headline}</h1>
          {texts && (
            <div className={styles.textsBlock}>
              {texts.paragraph &&
                texts.paragraph.map((paragraph, idx) => (
                  <p key={idx} className={styles.paragraph}>
                    {paragraph}
                  </p>
                ))}
              {texts.list && (
                <>
                  <h4 className={styles.listHeadLine}>{texts.list.headline}</h4>
                  <List listItems={texts.list.items} whiteDots />
                </>
              )}
            </div>
          )}
        </div>
        <div className={styles.blockMenuItems}>
          {subItems instanceof Array ? (
            subItems.map((subItem) => (
              <div key={subItem.id} className={styles.menuItemsHero}>
                {subItem.icon && <span className={styles.icon}>{subItem.icon}</span>}
                <span className={styles.name}>{subItem.name}</span>
              </div>
            ))
          ) : (
            <div className={styles.menuItemsHeroImage}>
              <img alt={subItems.alt} src={subItems.imageSrc} className={styles.image} />
            </div>
          )}
        </div>
        {subItems instanceof Array && <Button text={'ОБСУДИТЬ ПРОЕКТ'} width={365} />}
      </section>
    </ContainerFull>
  );
};

export default HeroBlock;

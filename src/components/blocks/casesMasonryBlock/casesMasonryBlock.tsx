import React, { FC } from 'react';
import Link from 'next/link';
import Masonry from 'react-masonry-component';

import styles from './casesMasonryBlock.module.css';

import { TCase } from '@/types/cases.type';
import Button from '@/components/buttons/button/button';
import { TServicesPageSlug } from '@/constans/dataPages';

interface IAllCasesBlock {
  cards: Array<TCase>;
  limit?: number;
  shuffle?: boolean;
  headline?: string;
  withButtonMore?: boolean;
  parentSlug?: TServicesPageSlug;
}

const masonryOptions = {
  fitWidth: false,
  gutter: 20,
  itemSelector: '.case-item',
};

const getChild = (data: IAllCasesBlock['cards'], limit: IAllCasesBlock['limit']) => {
  let arrayNodes: Array<React.ReactNode> = [];
  data.map((myCase) => {
    const child = (
      <li className={'case-item'} key={myCase.id}>
        <img className={styles.image} alt={myCase.image_alt} src={myCase.image_url_card} />
        <Link className={styles.link} href={{ pathname: '/case/' + myCase.slug }} />
        <div className={styles.itemInfo}>
          <span className={styles.category}>{myCase.category}</span>
          <span className={styles.headline}>{myCase.headline}</span>
        </div>
      </li>
    );
    arrayNodes.push(child);
  });
  if (limit) arrayNodes = arrayNodes.slice(0, limit);
  return arrayNodes;
};

const CasesMasonryBlock: FC<IAllCasesBlock> = ({
  cards,
  limit,
  headline,
  withButtonMore,
  parentSlug = '',
}) => {
  return (
    <>
      {headline && <h2 className={styles.headline_2}>{headline}</h2>}
      {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
      {/*// @ts-ignore*/}
      <Masonry
        className={styles.listCases}
        elementType={'ul'}
        options={masonryOptions}
        disableImagesLoaded={false}
        updateOnEachImageLoad={false}
      >
        {getChild(cards, limit)}
      </Masonry>
      {withButtonMore && (
        <Button width={'full'} href={`/cases/${parentSlug}`} text={'Посмотреть все кейсы'} />
      )}
    </>
  );
};

export default CasesMasonryBlock;

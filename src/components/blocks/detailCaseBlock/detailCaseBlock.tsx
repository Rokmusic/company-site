import React, { FC } from 'react';
import { useRouter } from 'next/router';

import styles from './detailCaseBlock.module.css';

import Container from '@/components/container/container';
import { TCase } from '@/types/cases.type';
import { TServicesPageSlug } from '@/constans/dataPages';
import CasesMasonryBlock from '@/components/blocks/casesMasonryBlock/casesMasonryBlock';
import OurCases from '@/components/blocks/ourCases/ourCases';
import ContainerFull from '@/components/containerFull/containerFull';
import useMediaQuery from '@/hooks/useMediaQuery';

type TCaseContent = {
  id: number;
  headline: string;
  paragraph: Array<string>;
  image_url?: string;
};

type TCaseDetail = {
  parentSlug: TServicesPageSlug;
  headline: string;
  image_url: string;
  tags: Array<string>;
  content: Array<TCaseContent>;
  more_cases: { cards: Array<TCase>; headline: string };
};

interface IDetailCaseBlockProps {
  data: TCaseDetail;
}

const DetailCaseBlock: FC<IDetailCaseBlockProps> = ({ data }) => {
  const isMobile = useMediaQuery('(max-width: 576px)');
  const cards = data.more_cases.cards;
  const galleryHeadline = data.more_cases.headline;
  const router = useRouter();
  const onClickHandler = () => {
    router.back();
  };
  return (
    <>
      <Container withoutMarginTop>
        <section className={styles.section}>
          <button className={styles.backButton} onClick={onClickHandler}>
            Вернуться
          </button>
          <h1 className={styles.headline}>{data.headline}</h1>
          <div className={styles.tags}>
            {data.tags.map((tag, index) => (
              <span key={index} className={styles.tag}>
                {tag}
              </span>
            ))}
          </div>
          <div className={styles.imageContainer}>
            <img className={styles.image} alt={data.headline} src={data.image_url} />
          </div>
          {data.content.map((content) => (
            <div className={styles.containerInfo} key={content.id}>
              <h2 className={styles.headline_2}>{content.headline}</h2>
              <div className={styles.containerTexts}>
                {content.paragraph.map((text, index) => (
                  <p key={index} className={styles.paragraph}>
                    {text}
                  </p>
                ))}
              </div>
              <div className={styles.imageContainer}>
                {content.image_url && (
                  <img className={styles.image} alt={content.headline} src={content.image_url} />
                )}
              </div>
            </div>
          ))}
        </section>
      </Container>
      <ContainerFull bgColor={'#111929'}>
        {isMobile ? (
          <section className={styles.sectionMasonry}>
            <CasesMasonryBlock
              cards={cards}
              limit={3}
              headline={galleryHeadline}
              parentSlug={data.parentSlug}
            />
          </section>
        ) : (
          <OurCases
            cards={cards}
            limit={3}
            headline={galleryHeadline}
            parentSlug={data.parentSlug}
          />
        )}
      </ContainerFull>
    </>
  );
};

export default DetailCaseBlock;

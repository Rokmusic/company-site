import React, { FC } from 'react';

import ListsBlock from '@/components/blocks/listsBlock/listsBlock';

const data = [
  {
    headline: 'АВТОМАТИЗАЦИЯ БИЗНЕС-ПРОЦЕССОВ',
    listItems: [
      'Контроль производственных задач',
      'Клиентский учет, лиды, допродажи',
      'Всегда доступный оперативный управленческий отчет в один клик.',
      'Автоматизация документооборота',
      'Регламентированный процесс работы',
    ],
    imageSrc: '/files/images/manufacturing.webp',
    link: '/automation-of-business-processes',
  },
  {
    headline: 'РАЗРАБОТКА ЦИФРОВЫХ\n СЕРВИСОВ',
    listItems: [
      'Проведем бизнес-анализ и грамотно спроектируем ваш цифровой продукт: от дизайна до запуска',
      'Воплотим в жизнь от аппаратно-программных решений до облачных платформ',
    ],
    imageSrc: '/files/images/software-code.webp',
    link: '/development-of-digital-services',
  },
  {
    headline: 'IoT, разработка программно-аппаратных решений',
    listItems: [
      'Проведем разработку электронно-механических устройств',
      'Реализуем программное решение для контроллеров и SoC',
      'Разработаем систему для управления и мониторинга электронно-механических устройств',
    ],
    imageSrc: '/files/images/board.webp',
    link: '/development-of-software-and-hardware-solutions_iot',
  },
];

const headline = 'Направления работы';

const WorkAreasBlock: FC = () => {
  return (
    <>
      <ListsBlock lists={data} headline={headline} />
    </>
  );
};

export default WorkAreasBlock;

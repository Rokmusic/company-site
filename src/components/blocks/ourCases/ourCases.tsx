import React, { FC } from 'react';
import Link from 'next/link';

import styles from './ourCases.module.css';

import Button from '@/components/buttons/button/button';
import { TCase } from '@/types/cases.type';

interface IOurCasesProps {
  cards: Array<TCase>;
  limit: number;
  parentSlug: string;
  headline?: string;
}

const getChild = (data: IOurCasesProps['cards'], limit: IOurCasesProps['limit']) => {
  let arrayNodes: Array<React.ReactNode> = [];
  data.map((myCase) => {
    const child = (
      <li className={styles.imageContainer} key={myCase.id}>
        <img className={styles.image} alt={myCase.image_alt} src={myCase.image_url_card} />
        <Link className={styles.link} href={{ pathname: '/case/' + myCase.slug }} />
        <div className={styles.itemInfo}>
          <span className={styles.category}>{myCase.category}</span>
          <span className={styles.headline}>{myCase.headline}</span>
        </div>
      </li>
    );
    arrayNodes.push(child);
  });
  if (limit) arrayNodes = arrayNodes.slice(0, limit);
  return arrayNodes;
};

const OurCases: FC<IOurCasesProps> = ({ cards, limit, headline, parentSlug }) => {
  return (
    <section className={styles.section}>
      <h2 className={styles.headline_2}>{headline}</h2>
      <ul className={styles.containerImages}>{getChild(cards, limit)}</ul>
      <Button text={'Посмотреть все кейсы'} width={'full'} href={`/cases/${parentSlug}`} />
    </section>
  );
};

export default OurCases;

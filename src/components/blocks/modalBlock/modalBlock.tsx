import React, { FC, useRef, useCallback } from 'react';

import styles from './modalBlock.module.css';

import Form from '@/components/form/form';
import { useAppDispatch } from '@/hooks/useAppDispatch';
import { useClickOutside } from '@/hooks/useClickOutside';
import { setIsOpen } from '@/store/slices/modalSlice';

const ModalBlock: FC = () => {
  const dispatch = useAppDispatch();
  const modalRef = useRef(null);
  function onClickHandler() {
    dispatch(setIsOpen(false));
  }
  const useCallBackHandler = useCallback(onClickHandler, [dispatch]);
  useClickOutside(modalRef, useCallBackHandler);
  return (
    <div ref={modalRef} className={styles.modal}>
      <Form headline={'Давайте обсудим ваш проект!'} />
    </div>
  );
};

export default ModalBlock;

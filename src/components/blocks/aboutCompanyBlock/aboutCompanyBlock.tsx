import React, { FC } from 'react';

import styles from './aboutCompanyBlock.module.css';

import ArrowTopRight from '@/components/icons/arrowTopRight';
import ContainerFull from '@/components/containerFull/containerFull';

const AboutCompanyBlock: FC = () => {
  return (
    <ContainerFull bgColor={'#F6F7F9'}>
      <section className={styles.section}>
        <div className={styles.paragraphContainer}>
          <h2 className={styles.headline_2}>о компании</h2>
          <p className={styles.paragraph}>
            Мы предоставляем услуги по разработке информационных систем, программно-аппаратных
            решений. Доводим проекты до конца и оказываем поддержку жизненного цикла
            <ArrowTopRight />
          </p>
        </div>
        <div className={styles.infoContainer}>
          <div className={styles.infoItem}>
            <span className={styles.count}>7+</span>
            <span className={styles.text}>лет на рынке</span>
          </div>
          <div className={styles.infoItem}>
            <span className={styles.count}>16+</span>
            <span className={styles.text}>сотрудников</span>
          </div>
          <div className={styles.infoItem}>
            <span className={styles.count}>60+</span>
            <span className={styles.text}>реализованных проектов</span>
          </div>
        </div>
      </section>
    </ContainerFull>
  );
};

export default AboutCompanyBlock;

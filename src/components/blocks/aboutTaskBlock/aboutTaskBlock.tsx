import React, { FC } from 'react';

import styles from './aboutTaskBlock.module.css';

import { contacts } from '@/constans/contacts';
import Container from '@/components/container/container';
import Form from '@/components/form/form';
import { getTelHref } from '@/utils/getTelHref';

const AboutTaskBlock: FC = () => {
  return (
    <Container>
      <section className={styles.section} id={'contacts'}>
        <h2 className={styles.headline_2}>давайте обсудим вашу задачу</h2>
        <div className={styles.contacts}>
          <div className={styles.contactsBlock}>
            <span className={styles.contactsName}>ПОЗВОНИТЬ</span>
            <span className={styles.contact}>
              <a href={getTelHref(contacts.tel)}>{contacts.tel}</a>
            </span>
          </div>
          <div className={styles.contactsBlock}>
            <span className={styles.contactsName}>НАПИСАТЬ</span>
            <span className={styles.contact}>
              {contacts.emails.map((email, index) => (
                <a key={index} href={`email:${email}`}>
                  {email}
                </a>
              ))}
            </span>
          </div>
          <div className={styles.contactsBlock}>
            <span className={styles.contactsName}>ПРИЙТИ</span>
            <span className={styles.contact}>{contacts.address}</span>
          </div>
        </div>
        <div className={styles.formContainer}>
          <Form />
        </div>
      </section>
    </Container>
  );
};

export default AboutTaskBlock;

import React, { FC } from 'react';

import styles from '@/components/form/form.module.css';
import ButtonClose from '@/components/buttons/buttonClose/buttonClose';
import Success from '@/components/icons/success';

interface IFormSuccessBlockProps {
  withHeadline: boolean;
  onClickHandlerCross: () => void;
}

const FormSuccessBlock: FC<IFormSuccessBlockProps> = ({ withHeadline, onClickHandlerCross }) => {
  return (
    <section className={styles.success}>
      {withHeadline && <ButtonClose onClickHandler={onClickHandlerCross} />}
      <div className={styles.successContent}>
        <Success />
        <h3 className={styles.successHeadline}>Мы свяжемся с вами в ближайшее время!</h3>
      </div>
    </section>
  );
};

export default FormSuccessBlock;

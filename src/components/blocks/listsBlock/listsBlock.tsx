import React, { FC } from 'react';

import styles from './listsBlock.module.css';

import ListsWithImage from '@/components/blocksParts/listsWithImage/listsWithImage';
import Container from '@/components/container/container';

import { IListWithImageItem } from '@/types/lists.types';

interface IListsBlockProps {
  lists: Array<IListWithImageItem>;
  headline?: string;
}

const ListsBlock: FC<IListsBlockProps> = ({ lists, headline }) => {
  return (
    <Container>
      {headline ? (
        <section className={styles.section}>
          {headline && <h2 className={styles.headline}>{headline}</h2>}
          <ListsWithImage data={lists} />
        </section>
      ) : (
        <div className={styles.section}>
          <ListsWithImage data={lists} />
        </div>
      )}
    </Container>
  );
};

export default ListsBlock;

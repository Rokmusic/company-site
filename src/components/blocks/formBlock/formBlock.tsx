import React, { FC, Dispatch, useState } from 'react';
import Link from 'next/link';

import styles from './formBlock.module.css';

import PhoneInput from '@/components/phoneInput/phoneInput';
import Button from '@/components/buttons/button/button';
import ButtonClose from '@/components/buttons/buttonClose/buttonClose';
import { postFormData, TPostData } from '@/store/slices/formSlice';
import { useAppDispatch } from '@/hooks/useAppDispatch';
import { setIsOpen } from '@/store/slices/modalSlice';

interface IFormBlockProps {
  headline?: string;
  setSuccess: Dispatch<React.SetStateAction<boolean>>;
  onClickHandlerCross: () => void;
}

const telRegExp = new RegExp(/[^0-9]/g);
const checkPhoneErrors = (phoneNumber: string) => {
  return !phoneNumber.length && phoneNumber.replace(telRegExp, '').length < 11;
};

const FormBlock: FC<IFormBlockProps> = ({ headline, setSuccess, onClickHandlerCross }) => {
  const [inputTel, setInputTel] = useState<string>('');
  const [inputName, setInputName] = useState<string>('');
  const [textarea, setTextarea] = useState<string>('');
  const [error, setError] = useState<string>('');
  const [isPosting, setIsPosting] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  const onSubmitHandler = async (event: React.FormEvent) => {
    event.preventDefault();
    if (checkPhoneErrors(inputTel)) return setError('Проверьте номер телефона');
    setIsPosting(true);
    const data: TPostData = {
      phone: inputTel.replace(telRegExp, ''),
      name: inputName,
      message: textarea,
    };
    const errorResponse = await dispatch(postFormData(data));
    if (errorResponse.payload) {
      setIsPosting(false);
      setError('Проверьте данные формы');
      return console.log('Ошибка API');
    }
    setIsPosting(false);
    return setSuccess(true);
  };

  const changeInputTelHandler = (text: string) => {
    setInputTel(text);
    if (error.length) setError('');
  };
  const onClickLinkHandler = () => {
    dispatch(setIsOpen(false));
  };

  return (
    <>
      {headline && (
        <div className={styles.headlineBlock}>
          <h3 className={styles.headline}>Давайте обсудим ваш проект!</h3>
          <ButtonClose onClickHandler={onClickHandlerCross} />
        </div>
      )}
      <form
        className={`${styles.form} ${isPosting ? styles.posting : ''}`}
        onSubmit={onSubmitHandler}
      >
        <label className={styles.label}>
          <span>ЧТОБЫ МЫ ПОЗВОНИЛИ ВАМ</span>
          {error && <span className={styles.error}>{error}</span>}
          <PhoneInput
            text={inputTel}
            setTextHandler={changeInputTelHandler}
            className={styles.formItem}
          />
        </label>
        <input
          className={styles.formItem}
          name={'name'}
          type={'text'}
          placeholder={'Представьтесь пожалуйста'}
          value={inputName}
          onChange={(e) => setInputName(e.currentTarget.value)}
          required
          maxLength={25}
          minLength={1}
        ></input>
        <textarea
          className={styles.formItem}
          placeholder={'Можете кратко описать суть вопроса'}
          value={textarea}
          onChange={(e) => setTextarea(e.currentTarget.value)}
          maxLength={500}
          minLength={1}
        />
        <Button text={'ОТПРАВИТЬ'} width={'full'} type={'submit'} />
      </form>
      <div>
        <span className={styles.spanPolicy}>
          Нажимая на кнопку "Отправить", вы соглашаетесь с&nbsp;
          <Link onClick={onClickLinkHandler} href={'/policy'} className={styles.link}>
            политикой обработки персональных данных
          </Link>
        </span>
      </div>
    </>
  );
};

export default FormBlock;

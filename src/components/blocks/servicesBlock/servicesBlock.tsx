import React, { FC } from 'react';

import styles from './servicesBlock.module.css';

import HeroBlock from '@/components/blocks/heroBlock/heroBlock';
import ListsBlock from '@/components/blocks/listsBlock/listsBlock';
import FullImageBlock from '@/components/blocks/fullImageBlock/fullImageBlock';
import TechnologyStackBlock from '@/components/blocks/technologyStackBlock/technologyStackBlock';
import useMediaQuery from '@/hooks/useMediaQuery';
import OurCases from '@/components/blocks/ourCases/ourCases';
import CasesMasonryBlock from '@/components/blocks/casesMasonryBlock/casesMasonryBlock';
import { TDataIoTPage, TDataAutomationPage, TDataDigitalPage } from '@/constans/dataPages';
import ContainerFull from '@/components/containerFull/containerFull';
import { TCase } from '@/types/cases.type';

interface IServicesBlock {
  data: TDataIoTPage | TDataAutomationPage | TDataDigitalPage;
  cards: Array<TCase>;
  additionalBlock?: 'technology-stack';
}

const ServicesBlock: FC<IServicesBlock> = ({ data, cards, additionalBlock }) => {
  const isMobile = useMediaQuery('(max-width: 576px)');
  data.fullImage.src = isMobile ? data.fullImage.srcMobile : data.fullImage.src;
  return (
    <>
      <HeroBlock
        headline={data.headline}
        subItems={data.rightBlock}
        bgImageStyle={data.bgImageStyle}
      />
      <ListsBlock lists={data.lists} />
      <FullImageBlock imageSrc={data.fullImage.src} alt={data.fullImage.alt} />
      {additionalBlock === 'technology-stack' && <TechnologyStackBlock />}
      {cards && (
        <ContainerFull bgColor={'#111929'}>
          {isMobile ? (
            <section className={styles.cases}>
              <CasesMasonryBlock
                limit={3}
                cards={cards}
                withButtonMore
                headline={'наши кейсы по автоматизации'}
              />
            </section>
          ) : (
            <OurCases
              cards={cards}
              limit={3}
              headline={'наши кейсы по автоматизации'}
              parentSlug={data.slug}
            />
          )}
        </ContainerFull>
      )}
    </>
  );
};

export default ServicesBlock;

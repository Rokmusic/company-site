import React, { FC } from 'react';

import styles from './fullImageBlock.module.css';

import ArrowTopRight from '@/components/icons/arrowTopRight';
import Container from '@/components/container/container';

interface IFullImageBlockProps {
  headline?: string;
  imageSrc: string;
  alt: string;
}

const FullImageBlock: FC<IFullImageBlockProps> = ({ headline, imageSrc, alt }) => {
  const As = headline ? 'section' : 'div';
  return (
    <Container>
      <As className={styles.section}>
        {headline && (
          <>
            <ArrowTopRight size={25} />
            <h3 className={styles.headline}>{headline}</h3>
          </>
        )}
        <div className={styles.container}>
          <img alt={alt} src={imageSrc} className={styles.image} />
        </div>
      </As>
    </Container>
  );
};

export default FullImageBlock;

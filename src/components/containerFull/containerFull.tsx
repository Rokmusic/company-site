import React, { FC } from 'react';

import styles from './containerFull.module.css';

interface IContainerFullProps {
  children: string | React.ReactNode | React.ReactNode[];
  bgImageStyle?: string;
  bgColor?: string;
}

const ContainerFull: FC<IContainerFullProps> = ({
  children,
  bgImageStyle,
  bgColor = '#F6F7F9',
}) => {
  const backGroundStyles = bgImageStyle
    ? { backgroundImage: bgImageStyle }
    : { background: bgColor };
  return (
    <div className={styles.container} style={backGroundStyles}>
      {children}
    </div>
  );
};

export default ContainerFull;

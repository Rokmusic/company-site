import React, { FC } from 'react';

import styles from './container.module.css';

interface IContainerProps {
  children: string | React.ReactNode | React.ReactNode[];
  withoutMarginTop?: boolean;
}

const Container: FC<IContainerProps> = ({ children, withoutMarginTop }) => {
  return (
    <div className={styles.container} style={withoutMarginTop ? { marginTop: 0 } : {}}>
      {children}
    </div>
  );
};

export default Container;

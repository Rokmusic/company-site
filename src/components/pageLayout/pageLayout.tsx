import React, { FC } from 'react';

import useScrollTopWhenChangeUrl from '@/hooks/useScrollTopWhenChangeUrl';

interface IMainLayout {
  children: string | React.ReactNode | React.ReactNode[];
}

const PageLayout: FC<IMainLayout> = ({ children }) => {
  useScrollTopWhenChangeUrl();
  return <>{children}</>;
};

export default PageLayout;

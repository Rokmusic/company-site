import React, { FC, SetStateAction, Dispatch } from 'react';

interface IBurgerProps {
  isOpenMenu: boolean;
  setIsOpenMenu: Dispatch<SetStateAction<boolean>>;
  classname: string;
}

const Burger: FC<IBurgerProps> = ({ isOpenMenu, setIsOpenMenu, classname }) => {
  const svgFirstRectPosition = {
    x: !isOpenMenu ? '4' : '6',
    y: !isOpenMenu ? '15' : '22.9727',
  };
  const svgSecondRectPosition = {
    x: !isOpenMenu ? '4' : '8.12109',
    y: !isOpenMenu ? '23' : '6',
  };
  const onClickHandler = () => {
    setIsOpenMenu((prevState) => !prevState);
  };
  return (
    <button
      className={classname + ` ${isOpenMenu ? 'burger-opened' : 'closed'}`}
      onClick={onClickHandler}
    >
      <svg
        width="32"
        height="32"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        {!isOpenMenu && <rect x="4" y="7" width="24" height="3" fill="#46A77D" />}
        <rect
          x={svgFirstRectPosition.x}
          y={svgFirstRectPosition.y}
          width="24"
          height="3"
          fill="#46A77D"
          transform={!isOpenMenu ? '' : 'rotate(-45 6 22.9727)'}
        />
        <rect
          x={svgSecondRectPosition.x}
          y={svgSecondRectPosition.y}
          width="24"
          height="3"
          fill="#46A77D"
          transform={!isOpenMenu ? '' : 'rotate(45 8.12109 6)'}
        />
      </svg>
    </button>
  );
};

export default Burger;

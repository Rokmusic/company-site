import React, { FC } from 'react';

const Logo: FC = () => {
  return (
    <svg
      width="79"
      height="14"
      viewBox="0 0 79 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    ></svg>
  );
};

export default Logo;

import React, { FC } from 'react';

interface IArrowTopRightProps {
  size?: number;
}

const ArrowTopRight: FC<IArrowTopRightProps> = ({ size = 45 }) => {
  return (
    <svg
      width={`${size}`}
      height={`${size}`}
      viewBox="0 0 45 45"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M2.00019 0.5H44.5002V43H39.5002V9.03553L3.76796 44.7678L0.232422 41.2322L35.9647 5.5H2.00019V0.5Z"
        fill="#46A77D"
      />
    </svg>
  );
};

export default ArrowTopRight;

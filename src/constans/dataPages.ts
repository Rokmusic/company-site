export type TServicesPageSlug =
  | 'automation-of-business-processes'
  | 'development-of-digital-services'
  | 'development-of-software-and-hardware-solutions_iot';

export const dataAutomationPage = {
  slug: 'automation-of-business-processes' as TServicesPageSlug,
  rightBlock: [
    {
      id: 1,
      name: 'CRM, ERP',
    },
    {
      id: 2,
      name: 'Индивидуальны подход',
    },
    {
      id: 3,
      name: 'Получите бесплатную консультацию по конкретно вашей задаче',
    },
  ],
  headline: 'автоматизация бизнес-процессов',
  bgImageStyle: `linear-gradient(180deg, rgba(17, 25, 41, 0.75) 50%, rgba(17, 25, 41, 0) 100%), url('/files/images/bg/automation-hero-bg.webp')`,
  lists: [
    {
      headline: 'Ваш бизнес может работать как часы',
      imageSrc: '/files/images/office-work.webp',
      listItems: [
        'контроль производственных задач',
        'бухгалтерский и финансовый учет',
        'складской учет',
        'клиентский учет, лиды, допродажи',
        'всегда доступный оперативный управленческий отчет в один клик',
      ],
    },
    {
      headline: 'Персонал может работать эффективнее за счет автоматизации рутинных вещей',
      imageSrc: '/files/images/personal-files.webp',
      listItems: [
        'автоматизация документооборота',
        'своевременное напоминание о задачах',
        'регламентированный процесс работы',
      ],
    },
  ],
  fullImage: {
    src: '/files/images/steps-automation.webp',
    srcMobile: '/files/images/steps-automation-mobile.webp',
    alt: 'seo alt',
  },
};

export const dataDigitalPage = {
  slug: 'development-of-digital-services' as TServicesPageSlug,
  rightBlock: [
    {
      id: 1,
      name: 'Проектирование, бизнес-анализ',
    },
    {
      id: 2,
      name: 'Прототипирование, разработка и сопровождение',
    },
    {
      id: 3,
      name: 'Получите бесплатную консультацию по конкретно вашей задаче',
    },
  ],
  headline: 'разработка цифровых сервисов',
  bgImageStyle: `linear-gradient(180deg, rgba(17, 25, 41, 0.75) 50%, rgba(17, 25, 41, 0) 165.75%), url('/files/images/bg/digital-services-hero-bg.webp')`,
  lists: [
    {
      headline: 'ВОТ ЧТО МЫ ПРЕДЛАГАЕМ',
      imageSrc: '/files/images/server-status.webp',
      listItems: [
        'Проведем бизнес-анализ и грамотно спроектируем ваш цифровой продукт: от дизайна до запуска',
        'Воплотим в жизнь от аппаратно-программных решений до облачных платформ',
      ],
    },
  ],
  fullImage: {
    src: '/files/images/steps-digital.webp',
    srcMobile: '/files/images/steps-digital-mobile.webp',
    alt: 'seo alt',
  },
};

export const dataIoTPage = {
  slug: 'development-of-software-and-hardware-solutions_iot' as TServicesPageSlug,
  rightBlock: [
    {
      id: 1,
      name: 'Схемотехника, подбор компонентов',
    },
    {
      id: 2,
      name: 'Разработка программного решения',
    },
    {
      id: 3,
      name: 'Получите бесплатную консультацию по конкретно вашей задаче',
    },
  ],
  headline: 'IT, разработка программно-аппаратных решений',
  bgImageStyle: `linear-gradient(180deg, rgba(17, 25, 41, 0.75) 50%, rgba(17, 25, 41, 0) 165.75%), linear-gradient(180deg, rgba(17, 25, 41, 0.75) 50%, rgba(17, 25, 41, 0) 100%), url('/files/images/bg/iot-hero-bg.webp')`,
  lists: [
    {
      headline: 'ВОТ ЧТО МЫ ПРЕДЛАГАЕМ',
      imageSrc: '/files/images/circuit-board.webp',
      listItems: [
        'Проведем разработку электронно-механических устройств',
        'Реализуем программное решение для контроллеров и SoC',
        'Разработаем систему для управления и мониторинга электронно-механических устройств',
      ],
    },
  ],
  fullImage: {
    src: '/files/images/steps-IoT.webp',
    srcMobile: '/files/images/steps-IoT-mobile.webp',
    alt: 'seo alt',
  },
};

export const allData = [dataIoTPage, dataAutomationPage, dataDigitalPage];

export const dataAboutCompanyPage = {
  headline: 'О компании',
  rightBlock: {
    imageSrc: '/files/images/team.webp',
    alt: 'seo alt',
  },
  texts: {
    paragraph: [
      'Мы - команда разработчиков с отлаженным циклом проектирования, производства и выпуска программного и аппаратного обеспечения для решения бизнес-задач.',
    ],
    list: {
      headline: 'Наши ключевые компетенции:',
      items: [
        'анализ требований и существующих систем, проектирование решений, написание технических заданий и технических проектов в области разработки программного и аппаратного обеспечения, сетевой инфраструктуры;',
        'разработка серверного программного обеспечения (основные инструменты - Erlang, Python), проектирование структуры СУБД (PostgreSQL, MySQL, sqlite), систем кеширования и in-memory БД (Redis);',
        'верстка, шаблонизация и реализация web-интерфейсов, в т.ч. адаптивных (ReactJS, VueJS);',
        'разработка мобильных приложений (Kotlin, Swift, Qt/QML);',
        'разработка и реализация программно-аппаратных комплексов, проектов IoT и многокомпонентных систем;',
        'построение систем автоматизированного тестирования и развертывания;',
        'управление ИТ-проектами и техническая поддержка.',
      ],
    },
  },
  bgColor: '#111929',
};

export const dataPolicyPage = {
  headline: 'ПОЛИТИКА в отношении обработки персональных данных',
  content: '',
};

const casesFirstAuto = [
  {
    id: 1,
    slug: 'test',
    category: 'Автоматизация',
    headline:
      'Проведение исследований на применение MPTCP для трансляции видео поверх нескольких каналов',
    image_url_card: '/files/images/fake2.webp',
    image_alt: 'seo alt',
  },
].reduce((res, current) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return res.concat(Array(8).fill(current));
}, []);

const casesSecondAuto = [
  {
    id: 1,
    slug: 'test2',
    category: 'Автоматизация',
    headline:
      'Проведение исследований на применение MPTCP для трансляции видео поверх нескольких каналов',
    image_url_card: '/files/images/fake.webp',
    image_alt: 'seo alt',
  },
].reduce((res, current) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return res.concat(Array(8).fill(current));
}, []);

const casesFirstDigit = [
  {
    id: 1,
    slug: 'test',
    category: 'Цифровые сервисы',
    headline:
      'Проведение исследований на применение MPTCP для трансляции видео поверх нескольких каналов',
    image_url_card: '/files/images/fake2.webp',
    image_alt: 'seo alt',
  },
].reduce((res, current) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return res.concat(Array(3).fill(current));
}, []);

const casesSecondDigit = [
  {
    id: 1,
    slug: 'test2',
    category: 'Цифровые сервисы',
    headline:
      'Проведение исследований на применение MPTCP для трансляции видео поверх нескольких каналов',
    image_url_card: '/files/images/fake.webp',
    image_alt: 'seo alt',
  },
].reduce((res, current) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return res.concat(Array(4).fill(current));
}, []);

const casesFirstIoT = [
  {
    id: 1,
    slug: 'test',
    category: 'IoT',
    headline:
      'Проведение исследований на применение MPTCP для трансляции видео поверх нескольких каналов',
    image_url_card: '/files/images/fake2.webp',
    image_alt: 'seo alt',
  },
].reduce((res, current) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return res.concat(Array(3).fill(current));
}, []);

const casesSecondIoT = [
  {
    id: 1,
    slug: 'test2',
    category: 'IoT',
    headline:
      'Проведение исследований на применение MPTCP для трансляции видео поверх нескольких каналов',
    image_url_card: '/files/images/fake.webp',
    image_alt: 'seo alt',
  },
].reduce((res, current) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return res.concat(Array(2).fill(current));
}, []);

export const fakeDataCardAutomation = [...casesFirstAuto, ...casesSecondAuto];
export const fakeDataCardDigit = [...casesFirstDigit, ...casesSecondDigit];
export const fakeDataCardIoT = [...casesFirstIoT, ...casesSecondIoT];

export type TDataAutomationPage = typeof dataAutomationPage;
export type TDataDigitalPage = typeof dataDigitalPage;
export type TDataIoTPage = typeof dataIoTPage;
export type TAllPageUnion = TDataAutomationPage | TDataDigitalPage | TDataIoTPage;
export type TDataAboutCompanyPage = typeof dataAboutCompanyPage;
export type TDataPolicyPage = typeof dataPolicyPage;

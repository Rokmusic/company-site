export type TContacts = {
  tel: string;
  emails: Array<string>;
  address: string;
};

export const contacts: TContacts = {
  tel: '+7 (961) 262 - 72 - 52',
  emails: ['vt@123.ru', 'nina@123.ru'],
  address: 'г. Тула, ул. Жуковского, д. 13, оф. 404',
};

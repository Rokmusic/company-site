import React from 'react';

import { TMenuItem } from '@/types/menu.type';
import Automation from '@/components/icons/automation';
import Monitor from '@/components/icons/monitor';
import Chip from '@/components/icons/chip';
import { TServicesPageSlug } from '@/constans/dataPages';

export const menuItems: Array<TMenuItem> = [
  {
    id: 1,
    name: 'Услуги',
    url: '#',
    items: [
      {
        id: '1_1',
        name: 'Автоматизация бизнес-процессов',
        url: 'automation-of-business-processes' as TServicesPageSlug,
        icon: <Automation />,
      },
      {
        id: '1_2',
        name: 'Разработка цифровых сервисов',
        url: 'development-of-digital-services' as TServicesPageSlug,
        icon: <Monitor />,
      },
      {
        id: '1_3',
        name: 'IoT, разработка программно-аппаратных решений',
        url: 'development-of-software-and-hardware-solutions_iot' as TServicesPageSlug,
        icon: <Chip />,
      },
    ],
  },
  {
    id: 2,
    name: 'О компании',
    url: 'about-company',
  },
  {
    id: 3,
    name: 'Кейсотека',
    url: 'cases',
  },
  {
    id: 4,
    name: 'Контакты',
    url: '#contacts',
  },
];

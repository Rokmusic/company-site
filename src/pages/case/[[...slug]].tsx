import React from 'react';
import { GetStaticPaths, NextPage, InferGetStaticPropsType, GetStaticPropsContext } from 'next';

import DetailCaseBlock from '@/components/blocks/detailCaseBlock/detailCaseBlock';
import { getMetadata } from '@/utils/getMetadata';
import { THeadMeta } from '@/types/cases.type';
import { TServicesPageSlug, fakeDataCardAutomation } from '@/constans/dataPages';

const apiBaseUrl = process.env.NEXT_PUBLIC_API_URL;

const data = {
  parentSlug: 'automation-of-business-processes' as TServicesPageSlug,
  headline:
    'Проведение исследований на применение MPTCP для трансляции видео поверх нескольких каналов',
  tags: ['Автоматизация', 'Автоматизация123213'],
  image_url: '/files/images/fakeDetails.webp',
  content: [
    {
      id: 1,
      headline: 'Заголовок 2 уровня',
      image_url: '/files/images/fakeDetails.webp',
      paragraph: [
        'Для мобильных трансляций видео в условиях отсутствия связи за исключением LTE/3G очень важным является обеспечение стабильной пропускной способности каналов. Эта задача решается агрегацией передающих каналов от нескольких операторов одновременно, но тривиальное объединение на канальном уровне обладает рядом серьезных технологических недостатков.Для решения подобных задач существует проект Multi-Path TCP (MPTCP), который позволяет организовывать подключение по множеству независимых uplink-ов и максимально утилизировать пропускную способность по их совокупности. Данный подход уже нашел применение в сетях операторов, предоставляющих гибридный доступ к сети (например, одновременно через LTE и DSL).',
      ],
    },
    {
      id: 2,
      headline: 'Заголовок 2 уровня',
      paragraph: [
        'Для мобильных трансляций видео в условиях отсутствия связи за исключением LTE/3G очень важным является обеспечение стабильной пропускной способности каналов. Эта задача решается агрегацией передающих каналов от нескольких операторов одновременно, но тривиальное объединение на канальном уровне обладает рядом серьезных технологических недостатков.Для решения подобных задач существует проект Multi-Path TCP (MPTCP), который позволяет организовывать подключение по множеству независимых uplink-ов и максимально утилизировать пропускную способность по их совокупности. Данный подход уже нашел применение в сетях операторов, предоставляющих гибридный доступ к сети (например, одновременно через LTE и DSL).',
        'Для мобильных трансляций видео в условиях отсутствия связи за исключением LTE/3G очень важным является обеспечение стабильной пропускной способности каналов. Эта задача решается агрегацией передающих каналов от нескольких операторов одновременно, но тривиальное объединение на канальном уровне обладает рядом серьезных технологических недостатков.Для решения подобных задач существует проект Multi-Path TCP (MPTCP), который позволяет организовывать подключение по множеству независимых uplink-ов и максимально утилизировать пропускную способность по их совокупности. Данный подход уже нашел применение в сетях операторов, предоставляющих гибридный доступ к сети (например, одновременно через LTE и DSL).',
        'Для мобильных трансляций видео в условиях отсутствия связи за исключением LTE/3G очень важным является обеспечение стабильной пропускной способности каналов. Эта задача решается агрегацией передающих каналов от нескольких операторов одновременно, но тривиальное объединение на канальном уровне обладает рядом серьезных технологических недостатков.Для решения подобных задач существует проект Multi-Path TCP (MPTCP), который позволяет организовывать подключение по множеству независимых uplink-ов и максимально утилизировать пропускную способность по их совокупности. Данный подход уже нашел применение в сетях операторов, предоставляющих гибридный доступ к сети (например, одновременно через LTE и DSL).',
      ],
    },
  ],
  more_cases: {
    headline: 'Еще кейсы по автоматизации',
    cards: fakeDataCardAutomation,
  },
};

type TDataCase = typeof data;
interface IDetailCasePropsChild {
  head: THeadMeta;
  caseData: TDataCase;
}
interface IDetailCaseProps {
  props: IDetailCasePropsChild;
}
type TDataCaseGetStaticProps = InferGetStaticPropsType<typeof getStaticProps>;

const DetailCase: NextPage<TDataCaseGetStaticProps> = (props) => {
  const caseData = props.caseData;
  return <>{caseData && <DetailCaseBlock data={caseData} />}</>;
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [
      {
        params: {
          slug: [],
        },
      },
    ],
    fallback: true,
  };
};

export const getStaticProps: (
  context: GetStaticPropsContext,
) => Promise<{ notFound: boolean } | IDetailCaseProps> = async (context) => {
  const { params } = context;
  const slug = params ? params.slug : '';
  const res = await fetch(`${apiBaseUrl}projects/${slug}`);
  if (res.status !== 200) {
    console.log(`Ошибка загрузки кейса ${res.status} | ${res.statusText || ''}`);
  }
  const caseData = await res.json();
  if (caseData?.detail) caseData.results = data;
  return {
    props: {
      caseData: caseData.results,
      head: getMetadata({
        title: 'seo title',
        description: 'data.desc',
      }),
    },
  };
};

export default DetailCase;

import React, { FC } from 'react';

import { Commissioner } from 'next/font/google';
const commissioner = Commissioner({ subsets: ['cyrillic'] });

const FontStyle: FC = () => {
  return (
    // eslint-disable-next-line react/no-unknown-property
    <style jsx global>{`
      html {
        font-family: ${commissioner.style.fontFamily};
      }
    `}</style>
  );
};

export default FontStyle;

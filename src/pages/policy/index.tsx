import React from 'react';
import { NextPage } from 'next';

import { dataPolicyPage } from '@/constans/dataPages';
import Container from '@/components/container/container';
import { getMetadata } from '@/utils/getMetadata';

const data = dataPolicyPage;

const Policy: NextPage = () => {
  return (
    <Container withoutMarginTop>
      <section className={'section__policy'}>
        <h4>{data.headline}</h4>
        <p className={'paragraph__policy'}>{data.content}</p>
      </section>
    </Container>
  );
};

export const getStaticProps = async () => {
  return {
    props: {
      head: getMetadata({
        title: 'Политика обработки персональных данных',
        description: 'desc',
      }),
    },
  };
};

export default Policy;

import React from 'react';
import { NextPage, GetStaticPaths, InferGetStaticPropsType, GetStaticPropsContext } from 'next';

import CasesPage from '@/components/pages/casesPage';
import { getMetadata } from '@/utils/getMetadata';
import { THeadMeta, TCase } from '@/types/cases.type';
import {
  fakeDataCardAutomation,
  fakeDataCardDigit,
  fakeDataCardIoT,
  allData,
  TServicesPageSlug,
  TAllPageUnion,
} from '@/constans/dataPages';

const getCards = (slug: Array<TServicesPageSlug>) => {
  let tempData: ICaseTabPropsChild['cards'];
  switch (slug[0]) {
    case 'automation-of-business-processes':
      tempData = fakeDataCardAutomation;
      break;
    case 'development-of-digital-services':
      tempData = fakeDataCardDigit;
      break;
    case 'development-of-software-and-hardware-solutions_iot':
      tempData = fakeDataCardIoT;
      break;
  }
  return tempData;
};

interface ICaseTabPropsChild {
  head: THeadMeta;
  cards: Array<TCase>;
}
interface ICaseTabProps {
  props: ICaseTabPropsChild;
}
type TDataCaseGetStaticProps = InferGetStaticPropsType<typeof getStaticProps>;

const CasesTab: NextPage<TDataCaseGetStaticProps> = (props) => {
  return <CasesPage cards={props.cards} />;
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = allData.map((service: TAllPageUnion) => {
    return {
      params: {
        slug: [service.slug as TServicesPageSlug],
      },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: (
  context: GetStaticPropsContext,
) => Promise<{ notFound: boolean } | ICaseTabProps> = async (context) => {
  const { params } = context;
  const slug = params ? params.slug : '';
  const cards = (getCards(slug as Array<TServicesPageSlug>) as ICaseTabPropsChild['cards']);
  return {
    props: {
      cards: cards,
      head: getMetadata({
        title: 'title',
        description: 'data.desc',
      }),
    },
  };
};

export default CasesTab;

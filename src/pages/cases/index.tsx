import React from 'react';
import { NextPage, InferGetStaticPropsType, GetStaticProps } from 'next';

import { getMetadata } from '@/utils/getMetadata';
import CasesPage from '@/components/pages/casesPage';
import { fakeDataCardAutomation, fakeDataCardDigit, fakeDataCardIoT } from '@/constans/dataPages';

const apiBaseUrl = process.env.NEXT_PUBLIC_API_URL;
const dataCards = [...fakeDataCardIoT, ...fakeDataCardAutomation, ...fakeDataCardDigit];

type TDataCaseGetStaticProps = InferGetStaticPropsType<typeof getStaticProps>;

const Cases: NextPage<TDataCaseGetStaticProps> = (props) => {
  return (
    <>
      <CasesPage cards={props.cards} />
    </>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  // const res = await fetch(`${apiBaseUrl}projects/`);
  // if (res.status !== 200) {
  //   console.log(`Ошибка загрузки карточек123123 ${res.status} | ${res.statusText || ''}`);
  // }
  // const data = await res.json();
  // return { notFound: true };
  const cards = dataCards;
  return {
    props: {
      cards: cards,
      head: getMetadata({
        title: 'title',
        description: 'data.desc',
      }),
    },
  };
};

export default Cases;

import React from 'react';
import { NextPage } from 'next';

import HeroBlock from '@/components/blocks/heroBlock/heroBlock';
import { getMetadata } from '@/utils/getMetadata';
import { dataAboutCompanyPage } from '@/constans/dataPages';

const data = dataAboutCompanyPage;

const AboutCompany: NextPage = () => {
  return (
    <>
      <HeroBlock
        headline={data.headline}
        subItems={data.rightBlock}
        bgColor={data.bgColor}
        texts={data.texts}
        withoutPaddingTop
      />
    </>
  );
};

export const getStaticProps = async () => {
  return {
    props: {
      head: getMetadata({ title: data.headline, description: data.headline }),
    },
  };
};

export default AboutCompany;

import React from 'react';
import { NextPage, InferGetStaticPropsType, GetStaticProps } from 'next';

import styles from './index.module.css';

import { menuItems } from '@/constans/menuData';
import WorkAreasBlock from '@/components/blocks/workAreasBlock/workAreasBlock';
import HeroBlock from '@/components/blocks/heroBlock/heroBlock';
import AboutCompanyBlock from '@/components/blocks/aboutCompanyBlock/aboutCompanyBlock';
import { getMetadata } from '@/utils/getMetadata';
import CasesMasonryBlock from '@/components/blocks/casesMasonryBlock/casesMasonryBlock';
import ContainerFull from '@/components/containerFull/containerFull';
import { fakeDataCardAutomation, fakeDataCardDigit, fakeDataCardIoT } from '@/constans/dataPages';

const apiBaseUrl = process.env.NEXT_PUBLIC_API_URL;
const headline = 'Модернизация и автоматизация бизнеса';
const bgImageStyle = `url('/files/images/bg/home-hero-bg.webp')`;
const menuWithSubItems = menuItems.filter((menuItem) => menuItem.items);

const dataCards = [...fakeDataCardIoT, ...fakeDataCardAutomation, ...fakeDataCardDigit];

type TDataCaseGetStaticProps = InferGetStaticPropsType<typeof getStaticProps>;

const Home: NextPage<TDataCaseGetStaticProps> = (props) => {
  return (
    <>
      {menuWithSubItems[0]?.items?.length && (
        <HeroBlock
          headline={headline}
          subItems={menuWithSubItems[0].items}
          bgImageStyle={bgImageStyle}
        />
      )}
      <AboutCompanyBlock />
      <WorkAreasBlock />
      <ContainerFull bgColor={'#111929'}>
        <section className={styles.sectionMasonry}>
          <CasesMasonryBlock
            limit={9}
            cards={props.cards}
            headline={'НАШИ ПРОЕКТЫ'}
            withButtonMore
          />
        </section>
      </ContainerFull>
    </>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  // const res = await fetch(`${apiBaseUrl}projects?limit=9`);
  // if (res.status !== 200) {
  //   console.log(`Ошибка загрузки карточек ${res.status} | ${res.statusText || ''}`);
  // }
  // const data = await res.json();
  const cards = dataCards;
  return {
    props: {
      cards: cards,
      head: getMetadata({
        title: 'seo title',
        description: 'data.desc',
      }),
    },
  };
};

export default Home;

import React from 'react';
import { NextPage, InferGetStaticPropsType, GetStaticPaths, GetStaticPropsContext } from 'next';

import { getMetadata } from '@/utils/getMetadata';
import ServicesBlock from '@/components/blocks/servicesBlock/servicesBlock';
import {
  dataAutomationPage,
  dataDigitalPage,
  dataIoTPage,
  TDataAutomationPage,
  TDataDigitalPage,
  TDataIoTPage,
  fakeDataCardAutomation,
  fakeDataCardDigit,
  fakeDataCardIoT,
  allData,
  TServicesPageSlug,
} from '@/constans/dataPages';

import { THeadMeta, TCase } from '@/types/cases.type';

interface IAutomationPropsChild {
  head: THeadMeta;
  cards: Array<TCase>;
  dataPage: TDataAutomationPage | TDataDigitalPage | TDataIoTPage | undefined;
}
interface IAutomationProps {
  props: IAutomationPropsChild;
}

type TDataCaseGetStaticProps = InferGetStaticPropsType<typeof getStaticProps>;

const getDataPage = (slug: Array<TServicesPageSlug>) => {
  let tempData: TDataAutomationPage | TDataDigitalPage | TDataIoTPage;
  if (!slug) return;
  switch (slug[0]) {
    case 'automation-of-business-processes':
      tempData = dataAutomationPage;
      break;
    case 'development-of-digital-services':
      tempData = dataDigitalPage;
      break;
    case 'development-of-software-and-hardware-solutions_iot':
      tempData = dataIoTPage;
      break;
  }
  return tempData;
};

const getCards = (slug: Array<TServicesPageSlug>) => {
  let tempData: Array<TCase>;
  switch (slug[0]) {
    case 'automation-of-business-processes':
      tempData = fakeDataCardAutomation;
      break;
    case 'development-of-digital-services':
      tempData = fakeDataCardDigit;
      break;
    case 'development-of-software-and-hardware-solutions_iot':
      tempData = fakeDataCardIoT;
      break;
  }
  return tempData;
};

const ServicesPage: NextPage<TDataCaseGetStaticProps> = (props) => {
  return <>{props.dataPage && <ServicesBlock data={props.dataPage} cards={props.cards} />}</>;
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = allData.map((service) => {
    return {
      params: {
        slug: [service.slug as TServicesPageSlug],
      },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: (
  context: GetStaticPropsContext,
) => Promise<{ notFound: boolean } | IAutomationProps> = async (context) => {
  const { params } = context;
  const slug = params ? params.slug : '';
  const dataPage = getDataPage(slug as Array<TServicesPageSlug>);
  const cards = getCards(slug as Array<TServicesPageSlug>) as Array<TCase>;
  return {
    props: {
      dataPage: dataPage,
      cards: cards,
      head: getMetadata({
        title: 'title',
        description: 'data.desc',
      }),
    },
  };
};

export default ServicesPage;

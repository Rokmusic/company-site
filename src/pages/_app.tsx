import React from 'react';

import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import Head from 'next/head';

import { store } from '@/store';

import './styles/globals.css';
import 'normalize.css';
import Modal from '@/components/modal/modal';
import FontStyle from '@/pages/styles/fontStyle';
import Header from '@/components/header/Header';
import Footer from '@/components/footer/footer';
import AboutTaskBlock from '@/components/blocks/aboutTaskBlock/aboutTaskBlock';
import PageLayout from '@/components/pageLayout/pageLayout';
import { IMetaItems } from '@/types/cases.type';

function Layout({ Component, pageProps }: AppProps) {
  return (
    <>
      <FontStyle />
      <Head>
        <title>{pageProps.head?.title + ' | siteName'}</title>
        {pageProps.head?.metas.map((attributes: IMetaItems, index: number) => (
          <meta {...attributes} key={index} />
        ))}
        <link rel="icon" href="/favicon_16x16.png" />
        <link rel="icon" href="/favicon_32x32.png" />
        <link rel="icon" href="/favicon_96x96.png" />
      </Head>
      <Provider store={store}>
        <PageLayout>
          <Header />
          <main className={'main'}>
            <Component {...pageProps} />
            <AboutTaskBlock />
          </main>
          <Footer />
        </PageLayout>
        <Modal />
      </Provider>
    </>
  );
}
export default Layout;

import { RootState } from '@/store';

export const modalStateSelector = (state: RootState) => {
  return state.modalStore.isOpen;
};

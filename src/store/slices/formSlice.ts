import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export type TPostData = {
  name: string;
  phone: string;
  message: string;
};

export const postFormData = createAsyncThunk('contactsForm/postData', async (data: TPostData) => {
  const response = await fetch('/post-feedback', {
    method: 'POST',
    headers: {
      'X-CSRFToken': 'aNEw91zdBdW5zSwQYIpawCMuihoJjG74gSj3pctC1FxErEWsNXEEyoiTx1au7S0s',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  const dataResponse = await response.json();
  if (response.status !== 201) return dataResponse;
  return false;
});

interface IModalSlice {
  status: 'idle' | 'pending' | 'succeeded' | 'failed';
}

const initialState = {
  status: 'idle',
} as IModalSlice;

const modalSlice = createSlice({
  name: 'modalSlice',
  initialState,
  reducers: {},
});

export default modalSlice.reducer;

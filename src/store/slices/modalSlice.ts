import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface IModalSlice {
  isOpen: boolean;
}

const initialState = {
  isOpen: false,
} as IModalSlice;

const modalSlice = createSlice({
  name: 'modalSlice',
  initialState,
  reducers: {
    setIsOpen: (state: IModalSlice, action: PayloadAction<boolean>) => {
      state.isOpen = action.payload;
    },
  },
});

export const { setIsOpen } = modalSlice.actions;
export default modalSlice.reducer;

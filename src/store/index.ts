import { configureStore } from '@reduxjs/toolkit';

import modalSlice from '@/store/slices/modalSlice';
import formSlice from '@/store/slices/formSlice';

export const store = configureStore({
  reducer: {
    modalStore: modalSlice,
    formStore: formSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

import React from 'react';

export type TMenu =
  | 'automation-of-business-processes'
  | 'development-of-digital-services'
  | 'development-of-software-and-hardware-solutions_iot'
  | 'about-company'
  | 'cases'
  | '#contacts'
  | '#';

export type TTabs = TMenu | 'all';

export type TMenuSubItems = {
  id: number | string;
  name: string;
  url: TMenu;
  icon?: React.ReactElement;
};

export type TMenuItem = {
  id: number | string;
  name: string;
  url: TMenu;
  items?: Array<TMenuSubItems>;
};

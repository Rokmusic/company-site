export interface IListWithImageItem {
  headline: string;
  listItems: Array<string>;
  imageSrc: string;
  link?: string;
}

export type IListWithImage = {
  data: Array<IListWithImageItem>;
};

import { TMenu } from '@/types/menu.type';

export type TCase = {
  id: number;
  slug: string;
  category: string;
  headline: string;
  image_url_card: string;
  image_alt: string;
};

export type TService = {
  id: number;
  slug: TMenu;
  category: string;
  cases: Array<TCase>;
};

export interface IMetaItems {
  name: string;
  property: string;
  content: string;
}

export type THeadMeta = {
  metas: ({ name: string; content: string } | { property: string; content: string })[];
  title: string;
};

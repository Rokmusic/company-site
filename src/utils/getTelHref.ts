import { TContacts } from '@/constans/contacts';

export const getTelHref = (tel: TContacts['tel']) => {
  return `tel:${tel.replace(/[-()\s]/g, '')}`;
};

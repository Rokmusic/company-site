interface IGetMetadata {
  title: string;
  description: string;
}

export const getMetadata = ({ title, description }: IGetMetadata) => {
  return {
    title: title,
    metas: [
      {
        name: 'description',
        content: description,
      },
      { property: 'og:title', content: title },
      {
        property: 'og:description',
        content: description,
      },
    ],
  };
};

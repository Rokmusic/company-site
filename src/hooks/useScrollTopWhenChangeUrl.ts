import { useRouter } from 'next/router';
import { useEffect } from 'react';

const useScrollTopWhenChangeUrl = () => {
  const router = useRouter();
  const handleRouteChange = () => {
    const body = document.querySelector('body');
    if (!body) return;
    body.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  };
  useEffect(() => {
    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);
};

export default useScrollTopWhenChangeUrl;
